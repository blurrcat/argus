Argus
=====

Generic time series data monitoring.

IT people collect metrics of their systems and manually set rules to get
alerts. For example, "if memory usage is more than 80% for 5 minutes, email me".
This kind of thresholds works for a small set of metrics. However, as systems
evolve, a tremendous amount of data will be collected and finally there's the
point when you can't even name all the metrics, not to mention set rules on them.
The data just lies there, doing nothing.

To address this problem, Argus is created. As a user, you only need to push
data into it, and all the rest is done. Argus persists and constantly monitors
your data. In case of an alert, Argus may notify you with Email, SMS, etc.

Now, switch "IT people" and "machines" to "health care" and "people". As
wearables develop, we gain insight of the human body. What we've done with
machines once can now be applied to humans. The application is endless.

Features
--------
* Data collection
* Time series persistence
* Monitoring
* Alert notification

