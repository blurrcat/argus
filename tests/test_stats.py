#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest


def test_stats(influxdb_service, app, client, points):
    if not influxdb_service:
        pytest.skip('influxdb not available')
    r = client.post('/api/point/', json={
        'points': points,
    })
    assert r.status_code == 201
    # check if stats is available
    for name in (
            'request_size', 'request_count', 'response_time', 'response_size'):
        r = app.influxdb.query('select value from {}'.format(name))
        item = r[0]
        assert item['name'] == name
        assert len(item['points'])
