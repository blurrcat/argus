#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import os
from flask import json
from flask.testing import FlaskClient
from mock import Mock
import pytest
from argus.main import create_app


class JsonClient(FlaskClient):
    def open(self, *args, **kwargs):
        json_data = kwargs.pop('json', None)
        if json_data:
            kwargs['data'] = json.dumps(json_data)
        headers = kwargs.setdefault('headers', {})
        headers['Content-Type'] = 'application/json'
        resp = super(JsonClient, self).open(*args, **kwargs)
        try:
            resp.json = json.loads(resp.data)
        except ValueError:
            pass
        return resp


@pytest.fixture
def app(request):
    logging.basicConfig(level=logging.DEBUG)
    os.environ['ARGUS_INFLUXDB_DATABASE'] = 'argus_test'
    _app = create_app()
    _app.config.debug = True
    _app.config.testing = True
    _app.test_client_class = JsonClient

    context = _app.app_context()
    context.push()

    def teardown():
        _app.influxdb.delete_database(_app.config['INFLUXDB_DATABASE'])
        context.pop()

    request.addfinalizer(teardown)

    return _app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def tags():
    return [{
        'key': 'host',
        'value': 'mac',
    }, {
        'key': 'region',
        'value': 'sg',
    }]


@pytest.fixture
def timed_points():
    return [{
        'name': 'cpu',
        'value': 0.5,
        'timestamp': '2015-01-29T21:50:44Z'
    }, {
        'name': 'cpu',
        'value': 0.5,
        'timestamp': '2015-01-29T21:50:44Z'
    }]


@pytest.fixture
def points():
    return [{
        'name': 'cpu',
        'value': 0.5,
    }, {
        'name': 'cpu',
        'value': 0.5,
    }]


@pytest.fixture(autouse=True)
def influxdb_service(request, monkeypatch):
    if os.environ.get('CI_INFLUXDB', False):
        print('influxdb not available: mock it')
        for method in ('write_points', 'create_database', 'delete_database'):
            monkeypatch.setattr(
                'influxdb.influxdb08.client.InfluxDBClient.%s' % method,
                Mock()
            )
        return False
    else:
        print('influxdb available: clean it')
        return True
