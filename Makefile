.PHONY: all test install uninstall coverage clean-pyc clean-build

all:
	@echo "install"
	@echo "test"
	@echo "clean"
	@echo "uninstall"
	@echo "coverage"

install: clean
	pip install -e .

uninstall: clean
	pip uninstall -y -r requirements.txt

test:
	coverage run --source argus -m py.test -l -r tests
	@coverage report

coverage: test
	@coverage html
	@echo "open htmlcov/index.html"

clean: clean-build clean-pyc

clean-build:
	@rm -fr *.egg-info
	@rm -fr htmlcov/

clean-pyc:
	@find . -type f -name "*.py[co]" -delete
	@find . -type d -name "__pycache__" -delete
