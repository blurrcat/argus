#! /usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup


with open('./requirements.txt') as f:
    install_requires = [l for l in f]

setup(
    name="argus",
    version='0.1.0',
    url='https://bitbucket.org/blurrcat/argus',
    author='blurrcat',
    author_email='blurrcat@gmail.com',
    license='MIT',
    packages=['argus'],
    install_requires=install_requires,
)
