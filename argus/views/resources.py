#!/usr/bin/env python
# -*- coding: utf-8 -*-
from collections import defaultdict
from datetime import datetime as _datetime
from dateutil import parser as d_parser
from dateutil.tz import tzutc
from flask import current_app
from flask.ext import restful
from webargs import Arg
from webargs.flaskparser import use_kwargs, parser


_epoch = _datetime(1970, 1, 1, tzinfo=tzutc())


def datetime(value):
    try:
        d = d_parser.parse(value)
    except AttributeError:
        raise ValueError
    return (d - _epoch).total_seconds()


class Points(restful.Resource):

    points_args = {
        'tags': Arg({
            'key': Arg(str, required=True),
            'value': Arg(str, required=True),
        }, multiple=True),
        'points': Arg({
            'name': Arg(str, required=True),
            'value': Arg(float, required=True),
        }, multiple=True, required=True),
    }

    @use_kwargs(points_args)
    def post(self, tags, points):
        items = [(t['key'], t['value']) for t in tags]
        items.append(('value', None))
        columns, point_tpl = zip(*items)
        tss = defaultdict(list)
        for p in points:
            point = list(point_tpl)
            point[-1] = p['value']
            tss[p['name']].append(point)
        data = [{
            'name': name,
            'columns': columns,
            'points': points
        } for name, points in tss.iteritems()]
        current_app.influxdb.write_points(data, time_precision='s')
        return None, 201


class TimedPoints(restful.Resource):

    points_args = {
        'tags': Arg({
            'key': Arg(str, required=True),
            'value': Arg(str, required=True),
        }, multiple=True),
        'points': Arg({
            'name': Arg(str, required=True),
            'timestamp': Arg(datetime, required=True),
            'value': Arg(float, required=True),
        }, multiple=True, required=True),
    }

    @use_kwargs(points_args)
    def post(self, tags, points):
        items = [(t['key'], t['value']) for t in tags]
        items.extend([('time', None), ('value', None)])
        columns, point_tpl = zip(*items)
        tss = defaultdict(list)
        for p in points:
            point = list(point_tpl)
            point[-2], point[-1] = p['timestamp'], p['value']
            tss[p['name']].append(point)
        data = [{
            'name': name,
            'columns': columns,
            'points': points
        } for name, points in tss.iteritems()]
        current_app.influxdb.write_points(data, time_precision='s')
        return None, 201


@parser.error_handler
def handle_request_parsing_error(err):
    """webargs error handler that uses Flask-RESTful's abort function to return
    a JSON error response to the client.
    """
    code = getattr(err, 'status_code', 400)
    msg = getattr(err, 'message', 'Invalid Request')
    restful.abort(code, message=msg)
